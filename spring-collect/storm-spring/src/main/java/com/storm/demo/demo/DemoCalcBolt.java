package com.storm.demo.demo;

import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichBolt;
import org.apache.storm.tuple.Tuple;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

public class DemoCalcBolt extends BaseRichBolt {
    static Logger logger = LoggerFactory.getLogger(DemoCalcBolt.class);
    private String propPath;
    private OutputCollector collector;

    public DemoCalcBolt(String propPath){
        this.propPath = propPath;
    }

    @Override
    public void prepare(Map map, TopologyContext topologyContext, OutputCollector outputCollector){
        this.collector = outputCollector;
//        BeanRepository.initialize(propPath,RedisFilterConfig.class,NotifyConfig.class,RedisDataConfig.class,RedisLimitConfig.class);
//        this.onceFilterService = BeanRepository.getBean(OnceFilterService.class);

    }


    /**
     *活动组全部走完，在活动组里面，一个活动匹配跳出这个活动组
     */
    @Override
    public void execute(Tuple input){
        try{
            byte[] data = input.getBinary(0);
        }catch(Exception ex){
            logger.error("",ex);
        }finally{
            collector.ack(input);
        }
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer outputFieldsDeclarer) {

    }
}
