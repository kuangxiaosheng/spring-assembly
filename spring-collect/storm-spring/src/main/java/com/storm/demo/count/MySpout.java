package com.storm.demo.count;

import org.apache.storm.spout.SpoutOutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichSpout;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Values;

import java.util.Map;

public class MySpout extends BaseRichSpout {


    SpoutOutputCollector collector;

    /**
     * 初始化方法
     * @param map
     * @param topologyContext
     * @param collector
     */
    @Override
    public void open(Map map, TopologyContext topologyContext, SpoutOutputCollector collector) {
        this.collector = collector;
    }


    /**
     *
     * storm 框架在while（true）在调用nextTuple方法
     *
     */
    @Override
    public void nextTuple() {

        collector.emit(new Values("i am lilei love hanmeimei"));
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer outputFieldsDeclarer) {
        outputFieldsDeclarer.declare(new Fields("love"));
    }









}
