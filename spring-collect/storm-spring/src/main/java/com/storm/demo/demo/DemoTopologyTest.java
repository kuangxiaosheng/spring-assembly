package com.storm.demo.demo;

public class DemoTopologyTest {

    /**
     * 本地模式的启动方式
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        DemoTopology demoTopology = new DemoTopology();
        demoTopology.runLocal(args);
    }

}
