package com.storm.demo.count;

import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichBolt;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;

import java.util.Map;

public class MysplitBolt extends BaseRichBolt {


    OutputCollector collector;

    /**
     * 初始化方法
     * @param map
     * @param topologyContext
     * @param collector
     */
    @Override
    public void prepare(Map map, TopologyContext topologyContext, OutputCollector collector) {
        this.collector = collector;
    }

    /**
     * 被storm框架while（TRUE） 循环调用  传入参数Tuple
     * @param input
     */
    @Override
    public void execute(Tuple input) {

        String line = input.getString(0);
//        String line = input.getStringByField("love");

        String[] arrWords = line.split(" ");

        for (String word : arrWords){
            collector.emit(new Values(word, 1));
        }

    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer outputFieldsDeclarer) {
        outputFieldsDeclarer.declare(new Fields("word", "num"));
    }
}
