package com.storm.demo.demo;


import org.apache.storm.kafka.KafkaSpout;
import org.apache.storm.kafka.SpoutConfig;
import org.apache.storm.spout.SpoutOutputCollector;
import org.apache.storm.task.TopologyContext;
import org.joda.time.LocalTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.Map;
import java.util.concurrent.TimeUnit;

public class DemoKafkaSpout extends KafkaSpout {

    private LocalTime startTime;
    private LocalTime endTime;

    public DemoKafkaSpout(SpoutConfig spoutConf,String startTime,String endTime) {
        super(spoutConf);
        DateTimeFormatter fmt = DateTimeFormat.forPattern("HH:mm:ss");
        this.startTime = fmt.parseLocalTime(startTime);
        this.endTime = fmt.parseLocalTime(endTime);
    }

    @Override
    public void open(Map conf, TopologyContext context, SpoutOutputCollector collector){
        super.open(conf,context,collector);
    }

    @Override
    public void nextTuple(){
        super.nextTuple();
    }

    private void waitTimeWindow(){
        while(true){
            LocalTime now = LocalTime.now();
            if(now.isAfter(startTime) && now.isBefore(endTime)){
                return;
            }else{
                try{
                    TimeUnit.MINUTES.sleep(1);
                }catch(InterruptedException e){
                }
            }
        }
    }




}
