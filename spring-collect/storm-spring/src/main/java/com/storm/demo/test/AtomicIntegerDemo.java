package com.storm.demo.test;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicInteger;

public class AtomicIntegerDemo {

    final static int clientTotal = 50000;
    public static AtomicInteger count = new AtomicInteger(0);


    /**
     * 分布式加锁自增长，达到某个值进行还原到指定值
     * @param args
     * @throws Exception
     */
    public static void main(String[] args){

        ExecutorService executorService = Executors.newCachedThreadPool();
        final Semaphore semaphore = new Semaphore(10);
        final CountDownLatch countDownLatch = new CountDownLatch(clientTotal);
        for (int i = 0; i < clientTotal ; i++) {
            executorService.execute(() -> {
                try {
                    semaphore.acquire();
                    count.incrementAndGet();
                    count.compareAndSet(100,1);
                    System.out.println("count1:{}"+ count.get());
                    semaphore.release();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                countDownLatch.countDown();
            });
        }
        executorService.shutdown();
        System.out.println("count:{}"+ count.get());

    }


}
