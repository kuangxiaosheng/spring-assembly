package com.spring.demo.demo;

import com.spring.demo.entity.Demo;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class DemoTest {

    public static void main(String[] args) throws Exception {

        ApplicationContext context = new ClassPathXmlApplicationContext("beans-spring.xml");

        Demo demo = context.getBean("demo", Demo.class);

        System.out.println(demo);



    }


}
