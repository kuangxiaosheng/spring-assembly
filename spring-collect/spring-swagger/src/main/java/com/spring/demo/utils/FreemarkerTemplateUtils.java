package com.spring.demo.utils;

import freemarker.template.Configuration;
import freemarker.template.Template;

import java.io.StringReader;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

/**
 * 本类演示freemarker模版：实现动态填充字符串模板
 * 实例地址：https://blog.csdn.net/abc231218/article/details/80624829
 */
public class FreemarkerTemplateUtils {


    public void demo () throws Exception {
        String strTemplate = "姓名：${name}；年龄：${age}"; // 测试模板数据（一般存储在数据库中）

        Map<String, Object> model = new HashMap<>();
        model.put("nane", "zhagnsan");
        model.put("age", 12);

        StringWriter out = new StringWriter();
        Configuration configuration = new Configuration();
        new Template("template", new StringReader(strTemplate),configuration ).process(model, out);

        System.out.println(out.toString());

    }


}
