package com.spring.demo.web;

import com.spring.demo.entity.Demo;
import io.swagger.annotations.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Api(value = "demoController相关api", tags = "demoController相关api")
@RestController
@RequestMapping("/api")
public class DemoController {



    @ApiOperation(value = "get请求演示第一种注解方式",notes = "get请求演示第一种注解方式")  //用在方法的上面，说明方法的作用
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "id编号")
    })
    @GetMapping(value = "/get/1")
    public ResponseEntity<?> getMethod1 (String id) {
        Map<String,Object> result = new HashMap<>();
        result.put("status","success");
        result.put("code", 200);
        result.put("data", id);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }


    /**
     * 如果是使用@ApiParam 默认是参数值requestbody中，所以必须加RequestParam声明请求参数是request参数
     */
    @ApiOperation(value = "get请求演示第二种注解方式",notes = "get请求演示第二种注解方式")  //用在方法的上面，说明方法的作用
    @GetMapping(value = "/get/2")
    public ResponseEntity<?> getMethod2 (@ApiParam(name = "id", value = "id编号") @RequestParam("id") String id) {
        Map<String,Object> result = new HashMap<>();
        result.put("status","success");
        result.put("code", 200);
        result.put("data", id);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }

    /**
     * post的参数允许的方式如下：
     *      1、前端传入的list放入到body体中
     *      2、前端传入一个map到body体中
     *      3、前端进行混合传入，request中有参数，body中也有参数
     *      4、使用model进行接收参数
     *
     *
     */
    @ApiOperation(value = "post的第一种玩法：前端传入的list放入到body体中")
    @PostMapping(value = "/post/1")
    public ResponseEntity<?> post1 ( @RequestBody @ApiParam(name = "id", value = "id编号", required = true) List<String> id) {
        Map<String,Object> result = new HashMap<>();
        result.put("status","success");
        result.put("code", 200);
        result.put("data", id);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }


    @ApiOperation(value = "post的第二种玩法：前端传入一个map到body体中")
    @ApiImplicitParams({
            @ApiImplicitParam( name = "param", value = "body体中的字段*id, *name", dataType = "Map") //value可以进行备注哪些是必须传入的，只是备注信息
    })
    @PostMapping(value = "/post/2")
    public ResponseEntity<?> post2 ( @RequestBody Map<String,Object> param) {
        Map<String,Object> result = new HashMap<>();
        result.put("status","success");
        result.put("code", 200);
        result.put("data", param);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }


    @ApiOperation(value = "post的第三种玩法：使用model进行接收参数")
    @ApiImplicitParams({
            @ApiImplicitParam( name = "param", value = "body体中的字段*id, *name"), //body;value可以进行备注哪些是必须传入的，只是备注信息
            @ApiImplicitParam( name = "requestParamsName", value = "request参数的名称") // request参数
    })
    @PostMapping(value = "/post/3")
    public ResponseEntity<?> post3 ( @RequestBody Map<String,Object> param, String requestParamsName) {
        param.put("requestParamsName", requestParamsName);
        Map<String,Object> result = new HashMap<>();
        result.put("status","success");
        result.put("code", 200);
        result.put("data", param);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }



    @ApiOperation(value = "post的第四种玩法：前端进行混合传入，request中有参数，body中也有参数")
    @PostMapping(value = "/post/4")
    public ResponseEntity<?> post4 (@RequestBody @ApiParam(name = "demo", value = "实例实体") Demo demo) {
        Map<String,Object> result = new HashMap<>();
        result.put("status","success");
        result.put("code", 200);
        result.put("data", demo);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }




}
