package com.spring.demo.entity;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value="demo实体", description = "demo实体")
public class Demo {


    @ApiModelProperty(name = "id", value = "demo编号", example = "1", required = true)
    private Long id;

    @ApiModelProperty(name = "name", value = "demo名称", example = "实例代码名字")
    private String name;

    @ApiModelProperty(name = "age", value = "年龄", example = "18")
    private int age;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
